package io.github.helmigandi.controller;

import io.github.helmigandi.dto.EmployeeDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/api/v1/redis")
public class HashController {
    private final RedisTemplate<String, Object> template;
    private static final String KEY = "redi2read:hash:";

    public HashController(RedisTemplate<String, Object> redisTemplate) {
        this.template = redisTemplate;
    }

    @PostMapping("/hash")
    @ResponseStatus(HttpStatus.CREATED)
    public void setStringRedis(@RequestBody List<EmployeeDto> employeeDtoList) {
        employeeDtoList
                .forEach(employeeDto -> template.opsForHash().put(KEY, employeeDto.getId(), employeeDto));
        template.expire(KEY, Duration.ofSeconds(30L));
    }

    @GetMapping("/hash/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Object setStringRedis(@PathVariable String id) {
        return template.opsForHash().get(KEY, Long.valueOf(id));
    }
}
