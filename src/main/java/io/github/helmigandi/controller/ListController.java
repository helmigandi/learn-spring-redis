package io.github.helmigandi.controller;

import io.github.helmigandi.dto.EmployeeDto;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.util.List;

@RestController
@RequestMapping("/api/v1/redis")
public class ListController {
    private final RedisTemplate<String, String> template;
    private static final String STRING_KEY_PREFIX = "redi2read:list:";

    public ListController(RedisTemplate<String, String> redisTemplate) {
        this.template = redisTemplate;
    }

    @PostMapping("/list")
    @ResponseStatus(HttpStatus.CREATED)
    public List<EmployeeDto> setStringRedis(@RequestBody List<EmployeeDto> employeeDtoList) {
        employeeDtoList.forEach(employeeDto -> template.opsForList().leftPush("MY-LIST", employeeDto.toString()));
        template.expire("MY-LIST", Duration.ofSeconds(60L));
        return employeeDtoList;
    }

    @GetMapping("/list")
    @ResponseStatus(HttpStatus.CREATED)
    public List<String> setStringRedis() {
        return template.opsForList().range("MY-LIST", 0L, -1L);
    }
}
