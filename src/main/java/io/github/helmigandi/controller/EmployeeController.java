package io.github.helmigandi.controller;

import io.github.helmigandi.dto.EmployeeDto;
import io.github.helmigandi.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/employees")
public class EmployeeController {
    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void save(@RequestBody List<EmployeeDto> employeeDtoList) {
        employeeService.save(employeeDtoList);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<EmployeeDto> getAll() {
        return employeeService.getAll();
    }

    @PostMapping("/hash")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveEmployeeList(@RequestBody List<EmployeeDto> employeeDtoList) {
        employeeService.create(employeeDtoList);
    }

    @GetMapping("/hash")
    @ResponseStatus(HttpStatus.OK)
    public List<Object> getAllEmployeeList() {
        return employeeService.getAllEmployeeList();
    }
}
