package io.github.helmigandi.controller;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.Duration;
import java.util.AbstractMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/redis")
public class RedisController {
    private final RedisTemplate<String, String> template;
    private static final String STRING_KEY_PREFIX = "redi2read:strings:";

    public RedisController(RedisTemplate<String, String> redisTemplate) {
        this.template = redisTemplate;
    }

    @PostMapping("/strings")
    @ResponseStatus(HttpStatus.CREATED)
    public Map.Entry<String, String> setStringRedis(@RequestBody Map.Entry<String, String> kvp) {
        template.opsForValue().set(STRING_KEY_PREFIX + kvp.getKey(), kvp.getValue());
        template.expire(STRING_KEY_PREFIX + kvp.getKey(), Duration.ofSeconds(15L));

        return kvp;
    }

    @GetMapping("/strings/{key}")
    public Map.Entry<String, String> getStringRedis(@PathVariable("key") String key) {
        String value = template.opsForValue().get(STRING_KEY_PREFIX + key);

        if (value == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "key not found");
        }

        return new AbstractMap.SimpleEntry<>(key, value);
    }

}
