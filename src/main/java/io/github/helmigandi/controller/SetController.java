package io.github.helmigandi.controller;

import io.github.helmigandi.dto.EmployeeDto;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.Duration;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/redis")
public class SetController {
    private final RedisTemplate<String, String> template;
    private static final String STRING_KEY_PREFIX = "redi2read:sets:";

    public SetController(RedisTemplate<String, String> redisTemplate) {
        this.template = redisTemplate;
    }

    @PostMapping("/sets")
    @ResponseStatus(HttpStatus.CREATED)
    public List<EmployeeDto> setStringRedis(@RequestBody List<EmployeeDto> employeeDtoList) {
        template.expire(STRING_KEY_PREFIX, Duration.ofSeconds(30L));
        employeeDtoList.forEach(employeeDto -> template.opsForSet().add(STRING_KEY_PREFIX, employeeDto.toString()));

        return employeeDtoList;
    }
}
