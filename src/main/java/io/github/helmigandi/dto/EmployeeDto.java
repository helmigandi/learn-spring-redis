package io.github.helmigandi.dto;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;
import java.util.Objects;

//@RedisHash(timeToLive = 60L)
public class EmployeeDto implements Serializable {

    @Id
    private Long id;

    private String name;

    @Indexed
    private String nip;

    public EmployeeDto() {
    }

    public EmployeeDto(Long id, String name, String nip) {
        this.id = id;
        this.name = name;
        this.nip = nip;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeDto that = (EmployeeDto) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(nip, that.nip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, nip);
    }

    @Override
    public String toString() {
        return "EmployeeDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", nip='" + nip + '\'' +
                '}';
    }
}
