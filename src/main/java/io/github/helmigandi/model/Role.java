package io.github.helmigandi.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@Data
@Builder
@RedisHash(timeToLive = 30L)
public class Role {
    @Id
    private String id;

    private String name;
}
