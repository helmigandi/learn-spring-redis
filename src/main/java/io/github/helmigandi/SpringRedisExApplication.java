package io.github.helmigandi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SpringRedisExApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRedisExApplication.class, args);
	}

}
