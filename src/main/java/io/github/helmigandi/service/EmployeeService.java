package io.github.helmigandi.service;

import io.github.helmigandi.dto.EmployeeDto;
import lombok.SneakyThrows;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class EmployeeService {
    private final List<EmployeeDto> employees = new ArrayList<>();
    private final HashOperations<String, Long, Object> hashOps;

    public EmployeeService(RedisTemplate<String, ?> redisTemplate) {
        this.hashOps = redisTemplate.opsForHash();
    }

    @CacheEvict(value = "employees", allEntries = true)
    public void save(List<EmployeeDto> employeeDtoList) {
        employees.addAll(employeeDtoList);
        System.out.println(employees.size());
    }

    @SneakyThrows
    @Cacheable(value = "employees")
    public List<EmployeeDto> getAll() {
        Thread.sleep(1000L);
        return employees;
    }

    public void create(List<EmployeeDto> employeeDtoList) {
        employeeDtoList
                .forEach(employeeDto -> hashOps.put("EmployeeDto", employeeDto.getId(), employeeDto));
    }

    public List<Object> getAllEmployeeList() {
        return hashOps.values("EmployeeDto");
    }

    public Object getEmployeeHash(Long id) {
        return hashOps.get("EmployeeDto", id);
    }
}
