# Learning Redis with Spring Boot

## Important Links

- [https://stackoverflow.com/questions/51237672/spring-cache-all-elements-in-list-separately](https://stackoverflow.com/questions/51237672/spring-cache-all-elements-in-list-separately)
- [https://developer.redis.com/develop/java/redis-and-spring-course/lesson_9/](https://developer.redis.com/develop/java/redis-and-spring-course/lesson_9/)
- [https://stackoverflow.com/questions/37953019/wrongtype-operation-against-a-key-holding-the-wrong-kind-of-value-php](https://stackoverflow.com/questions/37953019/wrongtype-operation-against-a-key-holding-the-wrong-kind-of-value-php)
- [https://docs.spring.io/spring-data/data-redis/docs/current/reference/html/#redis](https://docs.spring.io/spring-data/data-redis/docs/current/reference/html/#redis)
- [https://stackoverflow.com/questions/8181768/can-i-set-a-ttl-for-cacheable](https://stackoverflow.com/questions/8181768/can-i-set-a-ttl-for-cacheable)
- [https://www.baeldung.com/spring-data-redis-tutorial](https://www.baeldung.com/spring-data-redis-tutorial)
- [https://www.digitalocean.com/community/tutorials/spring-boot-redis-cache](https://www.digitalocean.com/community/tutorials/spring-boot-redis-cache)
- [https://stackabuse.com/spring-boot-with-redis-hashoperations-crud-functionality/](https://stackabuse.com/spring-boot-with-redis-hashoperations-crud-functionality/)
- 